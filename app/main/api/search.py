from flask import (
    Blueprint, request, url_for, flash, render_template, redirect
)
from app.db import get_db

bp = Blueprint('search', __name__)

@bp.route('/search', methods=('GET', 'POST'))
def search():
    """Search post by title """
    if request.method == 'POST':
        find_title = request.form['title_search']
        db = get_db()
        error = None

        if not find_title:
            error = 'Please search something.'

        if error is None:
            post = db.execute(
                "SELECT * FROM post WHERE title = ?", (find_title,)
            ).fetchone()

            if post is None:
                error = 'Cannot find'
                flash(error)
                return redirect(url_for('search.search'))

            return render_template('search/single_post.html', post=post)

        flash(error)

    return render_template('search/search.html')