from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from app.main.api.auth import login_required
from app.db import get_db

bp = Blueprint('blog', __name__)


@bp.route('/')
def index_root():
    """List the first page with 10 articles """
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
        ' LIMIT 10 OFFSET 0'
    ).fetchall()
    return render_template('blog/index_root.html', posts=posts)


@bp.route('/p<int:id>')
def index(id):
    """List other pages """
    if id == 0:
        return redirect(url_for('blog.index_root'))
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
        ' LIMIT 10 OFFSET ?', (id*10,)
    ).fetchall()
    if len(posts) < 10:
        # List the last page
        return render_template('blog/index_last.html', posts=posts, pre_id=id-1)

    return render_template('blog/index.html', posts=posts, next_id=id+1, pre_id=id-1)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    """Create new post """
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, author_id)'
                ' VALUES (?, ?, ?)',
                (title, body, g.user['id'])
            )
            db.commit()
            return redirect(url_for('blog.index_root'))

    return render_template('blog/create.html')


def get_post(id):
    """Get detail of 1 post """
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, f"Post id {id} doesn't exist.")


    return post


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    """Update post """
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('blog.index_root'))

    return render_template('blog/update.html', post=post)


@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    """Delete post """
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.index_root'))


@bp.route('/<int:id>', methods=('GET',))
def post_single(id):
    """Show 1 post """
    db = get_db()
    post = db.execute(
        "SELECT * FROM post WHERE id = ?", (id,)
    ).fetchone()
    error = None

    if post is None:
        error = "Post doesn't exist."
        flash(error)
        return redirect(url_for('blog.index_root'))

    return render_template('blog/single_post.html', post=post)
