from flask import Blueprint, jsonify

from app.db import get_db

bp = Blueprint('call_json', __name__)


@bp.route('/call_json')
def call_json():
    db = get_db()
    cursor = db.cursor()
    cursor.execute(
        "SELECT title, body, created"
        " FROM post"
    )
    data = cursor.fetchall()

    response = {}
    i = 1
    for post in data:
        response[i] = ({
            "title": post[0],
            "body": post[1],
            "created": post[2]
        })
        i = i + 1
    db.close()
    return jsonify(response)
