import os

from flask import Flask

def create_app(test_config=None):
    # Create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    if test_config is None:
        # Load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # Load the test config if passed in
        app.config.from_mapping(test_config)

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/hidden')
    def hidden():
        return "You found a hidden treasure"

    # Init Flaskapp
    from app import db
    db.init_app(app)

    # Register all blueprint
    from app.main.api import auth
    app.register_blueprint(auth.bp)

    from app.main.api import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')

    from app.main.api import search
    app.register_blueprint(search.bp)

    from app.main.api import call_json
    app.register_blueprint(call_json.bp)

    return app
