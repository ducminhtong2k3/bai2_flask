# Mini_Project_2

Create Flaskapp by using Flask
Flaskapp has some features:
- Listing and paging posts from [Vnexpress](https://vnexpress.net/giao-duc)
- Search posts by title
- Allow user to create, edit, delete post with login account


## Usage
(assume that you already install docker and docker-compose in your PC)

- Run app in Docker
```bash
docker-compose up --build
```
